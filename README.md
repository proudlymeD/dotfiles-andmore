# GNU/Linux system dotfiles, customization, and more
### Last updated: 11/20/2022

These are my personal dot files and other config files. Please feel free to use any one of these if you like them. 

Everything in the main directory is actively maintained, 
while files in the *inactive/* directory are config files for software that I do not currently daily-drive.


## Notes on specific applications

### General notes

* There are individual readmes in case there are special notes I left.
* Make sure to change OpenWeatherMap API keys if you use any of the weather scripts.

### yambar

* yambar appeared to have changed its syntax in a recent version. Some example scripts from dnkl's repo might not work out-of-the-box.
* Beware of the syntax for nested tags.
* For windows/tags on sway, two tags are needed for windows that 1) have content but 2) are not currently in view.

### Gentoo-related

* Bluetooth won't work. I specifically disabled it. 
* Joypads/game controllers don't work yet.
