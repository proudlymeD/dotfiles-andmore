#
# ~/.bashrc
#

# Last checked on 11/07/2022
# Added a few convenient aliases, also automatically invokes remind (a calendar app) on launch

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#
# Change display name
#
#PS1='[\u@\h \W]\$ '
PS1='[Me+Gentoo@ \W]\$ '


#
# Environmental variables
#

#
# Personal aliases
#
# Mistyping ls
alias ls='ls --color=auto'
alias sl='ls --color=auto'
alias la='ls --color=auto'
# Add ssh-dss support for ssh
alias ssh='ssh -oHostKeyAlgorithms=+ssh-dss'
alias scp='scp -oHostKeyAlgorithms=+ssh-dss'
# Easy switch to japanese code:
alias jp='LANG=ja_JP.UTF-8' 

#
# Startup prompt below:
#
# Show calendar with remind
echo -e "See calendar (week and to-do):\n"
remind -cl+1 calendars.rem
echo -e "\n"
remind calendars.rem
# Echo a new line
echo -e "\n"
