## Kernel configurations

I use Gentoo as a daily driver, so customizing the kernel is something quite important. 
I'll do my best to upload the kernel configurations as well as stuff 
that I know each kernel supports/doesn't support.

### 6.0.7-gentoo-DellLatitude7390
* Boots with efibootmgr
* Supports wifi driver
* Supports ethernet options
* Supports screen/keyboard backlights
* DISABLED Bluetooth
* DISABLED all extra stuff related to Intel ME
* Game controller support is a little bit sus
