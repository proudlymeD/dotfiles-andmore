## Inactive & depreciated configs

I am not actively using software in this directory,
so the dotfiles are not guaranteed to work.

Please contact me if you need help with any one of these, 
chances are I might be able to work out something.
