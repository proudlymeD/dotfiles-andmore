## Sway

My current Wayland compositor/WM. I've used riverwm for quite a while now, but zig (which river is built on) is seriously messing with my package dependencies, so I've reverted to using sway for the time being.

Note that the autotiling script **DOES NOT** work well with starting windows in floating mode, so it is currently disabled. Just uncomment the line at the bottom of config to have it re-enabled.

The autotiling script is copied shamelessly from [this awesome repo](https://github.com/nwg-piotr/autotiling).
