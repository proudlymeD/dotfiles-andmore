## Yambar config files

I currently use yambar (codenamed f00bar) as my main bar, here is their [source code](https://codeberg.org/dnkl/yambar).

To use, copy one of the config files and rename it "config.yml".
